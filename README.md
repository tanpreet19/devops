devops-recruitment-tasks
========================

Please see the [wiki](https://gitlab.com/recruitment-tasks/devops/wikis/home)



Tanpreet Update:
----------------

Packer:
======
- I have uploaded the ansible nginx playbook from my s3 browser https://tanpreet19.s3.ap-south-1.amazonaws.com/nginx_playbook.yml
- I was having issue ngnix configuration after installation during therefore hashed out the start nginx task to atleast create AMI out of it because with configuration is not correct start of nginx is being failed then packer is also getting failed. I am not sure why this is happening i tried installing and configuring other base images it worked fine. 

- I have created apache playbook  https://tanpreet19.s3.ap-south-1.amazonaws.com/apache_playbook.yml.
- Same issue i have faced with apache configuration as well.

Packer.jason almost works fine if not dont use the task for start ngnix in ansible.

By default nginx or apache should listen on port 80 therefore i didnot do any changes on conf file. 


Terraform:
=========
- I have corrected the terraform codes. With "terraform apply" i was able to deploy instance with default vpc and subnets as already mentioned in the code.


Please check the commit "task1 task2 completed" and "task1_task2_completed".

I was not able to complete task3. Since i dont have required set of skills for creating rest api.
