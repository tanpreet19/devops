variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-1"
}

variable "vpc_cidr" {
  default = "10.8.0.0/16"
}
variable "private_subnets" {
  default = ["10.8.0.0/24", "10.8.1.0/24"]
}
variable "public_subnets" {
  default = ["10.8.10.0/24", "10.8.11.0/24"]
}
variable "database_subnets" {
  default = ["10.8.20.0/24", "10.8.21.0/24"]
}
variable "sshpubkey_file" {}

variable "instance_type" {
  default = "t2.micro"
}
